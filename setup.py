import subprocess
from setuptools import setup
from requirements import executables

for executable in executables:
    result = subprocess.run(
        ['bash', '-c', 'command -v {}'.format(executable)],
        stdout=subprocess.PIPE
    )
    if result.returncode != 0:
        raise EnvironmentError(
            'Package requires executable "{}"'
            ' to be available on the system!'.format(executable)
        )

setup()
